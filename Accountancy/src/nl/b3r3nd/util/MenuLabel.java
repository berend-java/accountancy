package nl.b3r3nd.util;

public class MenuLabel {
	public static final String PRODUCT = "Product";
	public static final String PRODUCTS  = "Product Overview";
	public static final String NEW_PRODUCT  = "New Product";
	
	public static final String STORE = "Store";
	public static final String STORES  = "Stores Overview";
	public static final String NEW_STORE  = "New Store";
	
	public static final String TRANSACTION = "Transaction";
	public static final String TRANSACTIONS  = "Transactions Overview";
	public static final String NEW_TRANSACTION  = "New Transaction";

}
