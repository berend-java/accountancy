package nl.b3r3nd.main;

import nl.b3r3nd.product.Product;
import nl.b3r3nd.store.Store;
import nl.b3r3nd.transaction.Transaction;
import nl.b3r3nd.transaction.Transaction;
import nl.b3r3nd.transaction.TransactionFactory;

public class TestData {

	public TestData() {
		Product product1 = new Product("Product 1", 10.00);
		Product product2 = new Product("Product 2", 12.50);
		Product product3 = new Product("Product 3", 6.99);
		Product product4 = new Product("Product 4", 8.45);
		Product product5 = new Product("Product 5", 11.00);
		Product product6 = new Product("Product 6", 15.00);
		Product product7 = new Product("Product 7", 3.00);
		
		Store store1 = new Store("Lidl","Emmeloord");
		Store store2 = new Store("Aldi","Emmloord");
		Store store3 = new Store("Albert Heijn","Emmeloord");
		Store store4 = new Store("Jumbo","Emmeloord");
		Store store5 = new Store("Jumbo","Zwolle");
		
		
		Transaction transaction = TransactionFactory.createSingleTransaction(store1, "Bier halen");
		transaction.addProduct(product1);
		transaction.addProduct(product2);
		transaction.addProduct(product5);
		
		Transaction transaction2 = TransactionFactory.createSingleTransaction(store2, "Alcohol?");
		transaction2.addProduct(product2);
		
		Transaction transaction3 = TransactionFactory.createSingleTransaction(store2, "Geen Idee");
		transaction3.addProduct(product3);
		
		Transaction transaction4 = TransactionFactory.createSingleTransaction(store3, "Wtf is this");
		transaction4.addProduct(product4);
		
		Transaction transaction5 = TransactionFactory.createSingleTransaction(store4, "Computer");
		transaction5.addProduct(product5);
		transaction5.addProduct(product1);
		transaction5.addProduct(product3);
		
		Transaction transaction6 = TransactionFactory.createSingleTransaction(store5, "Beeldscherm");
		transaction6.addProduct(product6);
		
		Transaction transaction7 = TransactionFactory.createSingleTransaction(store5, "Onzin");
		transaction7.addProduct(product7);
		transaction7.addProduct(product5);

		
		
	}
}
