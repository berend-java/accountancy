package nl.b3r3nd.main;

/**
 * 
 * @author Berend de Groot
 *
 */
public class Main {

	public static void main(String[] args) {
		if (Config.isTest()) {
			new TestData();
		}
		new Frame();
	}
}
