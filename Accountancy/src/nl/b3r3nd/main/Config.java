package nl.b3r3nd.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * Config singleton class to access properties set in config file.
 * I added static get and set functions which will call the getIntance in order 
 * to prevent doing Config.getInstance.getSomething(), instead I can call Config.getHeight()
 * even though the principe stays the same.
 * 
 * @author Berend de Groot
 */
public final class Config {
	private static Config instance;
	private int height;
	private int width;
	private Boolean testMode;
	

	private Config() {
		Properties properties = new Properties();

		try (InputStream input = new FileInputStream("config/config.properties")) {
			properties.load(input);
		} catch (IOException e) {
			System.out.println("Can't find configuration file.");
			;
		}
		
		this.width = Integer.parseInt(properties.getProperty("frame.width"));
		this.height = Integer.parseInt(properties.getProperty("frame.height"));
		this.testMode = Boolean.parseBoolean(properties.getProperty("app.test"));
		
	}
	
	public static Config getInstance() {
		if(instance == null) {
			instance = new Config();
		}
		return instance;
	}
	
	public static int getHeight() {
		return Config.getInstance().height;
	}
	
	public static int getWidth() {
		return Config.getInstance().width;
	}
	
	
	public static boolean isTest() {
		return Config.getInstance().testMode;
	}
		
}
