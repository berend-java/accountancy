package nl.b3r3nd.main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.*;

import nl.b3r3nd.product.NewProductPanel;
import nl.b3r3nd.product.ProductsPanel;
import nl.b3r3nd.store.NewStorePanel;
import nl.b3r3nd.store.StoresPanel;
import nl.b3r3nd.transaction.NewTransactionPanel;
import nl.b3r3nd.transaction.TransactionsPanel;
import nl.b3r3nd.util.MenuLabel;
/**
 * 
 * @author Berend de Groot
 *
 */
public class Frame extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private JMenuBar menuBar;
	private JMenu productMenu;
	private JMenuItem newProductMenuItem, overviewProductMenuItem;

	private JMenu storeMenu;
	private JMenuItem newStoreMenuItem, overviewStoreMenuItem;
	
	private JMenu transactionMenu;
	private JMenuItem newTransactionMenuItem, overviewTransactionMenuItem;
	
	/**
	 * Frame Constructor
	 * 
	 * @author Berend de Groot
	 */
	public Frame() {
		setSize(Config.getWidth(), Config.getHeight());
		
		setUndecorated(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Accountancy");

		menuBar = new JMenuBar();
		
		productMenu = new JMenu(MenuLabel.PRODUCT);
		newProductMenuItem = new JMenuItem(MenuLabel.NEW_PRODUCT);
		overviewProductMenuItem = new JMenuItem(MenuLabel.PRODUCTS);
		productMenu.add(newProductMenuItem);
		productMenu.add(overviewProductMenuItem);
		menuBar.add(productMenu);
		
		storeMenu = new JMenu(MenuLabel.STORE);
		newStoreMenuItem = new JMenuItem(MenuLabel.NEW_STORE);
		overviewStoreMenuItem = new JMenuItem(MenuLabel.STORES);
		storeMenu.add(newStoreMenuItem);
		storeMenu.add(overviewStoreMenuItem);
		menuBar.add(storeMenu);
		
		transactionMenu = new JMenu(MenuLabel.TRANSACTION);
		newTransactionMenuItem = new JMenuItem(MenuLabel.NEW_TRANSACTION);
		overviewTransactionMenuItem = new JMenuItem(MenuLabel.TRANSACTIONS);

		transactionMenu.add(newTransactionMenuItem);
		transactionMenu.add(overviewTransactionMenuItem);

		menuBar.add(transactionMenu);
		
		newProductMenuItem.addActionListener(this);
		overviewProductMenuItem.addActionListener(this);
		newStoreMenuItem.addActionListener(this);
		overviewStoreMenuItem.addActionListener(this);
		newTransactionMenuItem.addActionListener(this);
		overviewTransactionMenuItem.addActionListener(this);
		
		setJMenuBar(menuBar);
		
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		getContentPane().add(new DashboardPanel());
		
		setVisible(true);
	}
	


	@Override
	public void actionPerformed(ActionEvent e) {
		Container contentPane = getContentPane();
		contentPane.removeAll();
		
		if(e.getActionCommand().equals(MenuLabel.PRODUCTS)) {
			contentPane.add(new ProductsPanel());
		}
		if(e.getActionCommand().equals(MenuLabel.NEW_PRODUCT)) {
			contentPane.add(new NewProductPanel());
		}
		if(e.getActionCommand().equals(MenuLabel.STORES)) {
			contentPane.add(new StoresPanel());
		}
		if(e.getActionCommand().equals(MenuLabel.NEW_STORE)) {
			contentPane.add(new NewStorePanel());
		}
		if(e.getActionCommand().equals(MenuLabel.TRANSACTIONS)) {
			contentPane.add(new TransactionsPanel());
		}
		if(e.getActionCommand().equals(MenuLabel.NEW_TRANSACTION)) {
			contentPane.add(new NewTransactionPanel());
		}
		contentPane.revalidate();
		contentPane.repaint();
	}
}
