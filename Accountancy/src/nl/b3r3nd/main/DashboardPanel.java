package nl.b3r3nd.main;

import javax.swing.*;
/**
 * 
 * @author Berend de Groot
 *
 */
public class DashboardPanel extends JPanel {
	private static final long serialVersionUID = 7308454879307010238L;
	private JLabel label;

	public DashboardPanel() {
		label = new JLabel("Welcome Berend, time to manage some finances? Let me help you!");
		add(label);
	}

}
