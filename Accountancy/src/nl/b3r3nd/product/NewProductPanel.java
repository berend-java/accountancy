package nl.b3r3nd.product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/**
 * 
 * @author Berend de Groot
 *
 */
public class NewProductPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1707202805287908614L;
	private JLabel productNameLabel, productPriceLabel;
	private JTextField productNameInput, productPriceInput;
	private JButton submit;
	
	public NewProductPanel() {
		productNameLabel = new JLabel("Product Name: ");
		productNameInput = new JTextField(20);
		
		productPriceLabel = new JLabel("Product Price: ");
		productPriceInput = new JTextField(20);
		
		submit = new JButton("Save");
		submit.addActionListener(this);
		
		add(productNameLabel);
		add(productNameInput);
		add(productPriceLabel);
		add(productPriceInput);
		add(submit);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Save")) {
			String productName = productNameInput.getText();
			double productPrice = Double.valueOf(productPriceInput.getText());
			
			new Product(productName, productPrice);
		}
		
	}
}
