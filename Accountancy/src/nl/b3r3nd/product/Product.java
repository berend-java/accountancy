package nl.b3r3nd.product;

import java.util.ArrayList;

/**
 * 
 * @author Berend de Groot
 *
 */
public class Product {

	private static ArrayList<Product> products = new ArrayList<Product>();
	private String name;
	private double price;
	
	public Product(String name, double price) {
		setName(name);
		setPrice(price);
		addProduct(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public static void addProduct(Product product) {
		products.add(product);
	}
	
	public static ArrayList<Product> getProducts() {
		return products;
	}
	
	public String toString() {
		return "Name: " + name + " Price: " + price;
	}
	
}
