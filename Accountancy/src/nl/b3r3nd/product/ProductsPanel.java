package nl.b3r3nd.product;

import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
/**
 * 
 * @author Berend de Groot
 *
 */
public class ProductsPanel extends JPanel {
	private static final long serialVersionUID = 460392618210827191L;
	private JTable table;
	private JScrollPane scrollPane;
	
	public ProductsPanel() {
		DefaultTableModel tableModel = new DefaultTableModel(new String[] { "Name", "Price" }, 0);
		ArrayList<Product> products = Product.getProducts();
		
		for(Product product: products) {
			tableModel.addRow(new Object[]{product.getName(), product.getPrice()});
		}
		
		table = new JTable(tableModel);
		table.setFocusable(false);
		table.setRowSelectionAllowed(false);
		scrollPane = new JScrollPane(table);
		add(scrollPane);
	}
}
