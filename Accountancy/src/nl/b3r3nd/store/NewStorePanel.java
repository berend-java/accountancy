package nl.b3r3nd.store;

import javax.swing.*;
/**
 * 
 * @author Berend de Groot
 *
 */
public class NewStorePanel extends JPanel {
	private static final long serialVersionUID = 619013392402020493L;
	private JLabel label;
	
	public NewStorePanel() {
		label = new JLabel("New Store");
		add(label);
	}
}
