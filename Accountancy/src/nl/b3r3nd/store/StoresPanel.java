package nl.b3r3nd.store;

import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
/**
 * 
 * @author Berend de Groot
 *
 */
public class StoresPanel extends JPanel {
	private static final long serialVersionUID = 7016983704674331795L;
	private JTable table;
	private JScrollPane scrollPane;

	public StoresPanel() {
		DefaultTableModel tableModel = new DefaultTableModel(new String[] { "Name", "Location" }, 0);
		ArrayList<Store> stores = Store.getStores();
		
		for(Store store: stores) {
			tableModel.addRow(new Object[]{store.getName(), store.getLocation()});
		}
		
		table = new JTable(tableModel);
		table.setFocusable(false);
		table.setRowSelectionAllowed(false);
		scrollPane = new JScrollPane(table);
		add(scrollPane);
	}

}
