package nl.b3r3nd.store;

import java.util.ArrayList;

/**
 * 
 * @author Berend de Groot
 *
 */
public class Store {
	private String name;
	private String location;
	private static ArrayList<Store> stores = new ArrayList<Store>();
	
	
	public Store(String name, String location) {
		setName(name);
		setLocation(location);
		addStore(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public static void addStore(Store store) {
		stores.add(store);
	}
	
	public static ArrayList<Store> getStores() {
		return stores;
	}
	
	public String toString() {
		return name + ", " + location;
	}
}
