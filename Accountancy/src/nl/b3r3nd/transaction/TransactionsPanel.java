package nl.b3r3nd.transaction;

import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author Berend de Groot
 *
 */
public class TransactionsPanel extends JPanel {
	private static final long serialVersionUID = -2153868867663186458L;
	private JTable table;
	private JScrollPane scrollPane;

	public TransactionsPanel() {
		DefaultTableModel tableModel = new DefaultTableModel(
				new String[] { "Description", "Store", "Products", "Total Price" }, 0);
		ArrayList<Transaction> transactions = Transaction.getTransactions();

		for (Transaction transaction : transactions) {
			tableModel.addRow(new Object[] { transaction.getDescription(), transaction.getStore().toString(),
					transaction.getAmountOfProducts(), transaction.getTotalPrice() });
		}

		table = new JTable(tableModel);
		table.setFocusable(false);
		table.setRowSelectionAllowed(false);
		scrollPane = new JScrollPane(table);
		add(scrollPane);
	}
}
