package nl.b3r3nd.transaction;

import java.util.ArrayList;

import nl.b3r3nd.product.Product;
import nl.b3r3nd.store.Store;

/**
 * 
 * @author Berend de Groot
 *
 */
public class Transaction {
	private Store store;
	private String description;
	private boolean monthly;
	private static ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	private ArrayList<Product> products;
	
	public Transaction() {
		products = new ArrayList<Product>();
		addTransaction(this);
	}


	public Store getStore() {
		return store;
	}


	public void setStore(Store store) {
		this.store = store;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public boolean isMonthly() {
		return monthly;
	}


	public void setMonthly() {
		this.monthly = true;
	}
	
	public void setSingle() {
		this.monthly = false;
	}


	public ArrayList<Product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public void addProduct(Product product) {
		products.add(product);
	}

	
	public static void addTransaction(Transaction transaction) {
		transactions.add(transaction);
	}
	
	public static ArrayList<Transaction> getTransactions() {
		return transactions;
	}
	
	public int getAmountOfProducts() {
		return products.size();
	}
	
	public double getTotalPrice() {
		double totalPrice = 0;
		for(Product product: products) {
			totalPrice += product.getPrice();
		}
		
		return totalPrice;
	}
	
	
}
