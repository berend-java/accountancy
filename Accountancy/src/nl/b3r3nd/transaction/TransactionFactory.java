package nl.b3r3nd.transaction;

import nl.b3r3nd.store.Store;

public class TransactionFactory {

	public static Transaction createSingleTransaction(Store store, String description) {
		Transaction transaction = new Transaction();
		transaction.setMonthly();
		transaction.setStore(store);
		transaction.setDescription(description);
		return transaction;
	}
	
	public static Transaction createMonthlyTransaction(Store store, String description) {
		Transaction transaction = new Transaction();
		transaction.setSingle();
		transaction.setStore(store);
		transaction.setDescription(description);
		return transaction;
	}
}
