package nl.b3r3nd.transaction;

import javax.swing.*;
/**
 * 
 * @author Berend de Groot
 *
 */
public class NewTransactionPanel extends JPanel {
	private static final long serialVersionUID = 1968548667765196792L;
	private JLabel label;
	
	public NewTransactionPanel() {
		label = new JLabel("New Transaction");
		add(label);
	}

}
